var cdbauthdialogJS = {
  onLoad: function() {
    var dialogText = document.getElementById("cdbAuthDialogStrings").getString("authDialogText");
    dialogText = dialogText + " " + String(window.arguments[0].url);
    document.getElementById("cdbAuthDialogText").textContent = dialogText;
    document.getElementById("authDialogUsernameValue").value = String(window.arguments[0].username);
    document.getElementById("authDialogPasswordValue").value = String(window.arguments[0].password);
  },

  doOK: function() {
    window.arguments[0].username = String(document.getElementById("authDialogUsernameValue").value);
    window.arguments[0].password = String(document.getElementById("authDialogPasswordValue").value);
    window.arguments[0].doneOK = true;
    return true;
  }
};
