var cdbcommon = {
  EnforceLineEndgings: function(textLine) {
    var localText = String(textLine);
    var tempCode = String.fromCharCode(11);
    var localCode = String.fromCharCode(13);
    while (localText.indexOf("&#13;") >= 0) {localText = localText.replace("&#13;",localCode);}
    localCode = String.fromCharCode(10);
    while (localText.indexOf("&#10;") >= 0) {localText = localText.replace("&#10;",localCode);}
    localCode = String.fromCharCode(13, 10);
    while (localText.indexOf(localCode) >= 0) {localText = localText.replace(localCode,tempCode);}
    localCode = String.fromCharCode(13);
    while (localText.indexOf(localCode) >= 0) {localText = localText.replace(localCode,tempCode);}
    localCode = String.fromCharCode(10);
    while (localText.indexOf(localCode) >= 0) {localText = localText.replace(localCode,tempCode);}
    localCode = String.fromCharCode(13, 10);
    while (localText.indexOf(tempCode) >= 0) {localText = localText.replace(tempCode,localCode);}
    return localText;
  },

  Unfold: function(textLine) {
    var localText = String(textLine);
    var localEOL = String.fromCharCode(13, 10, 32); // CR+LF+SP
    while (localText.indexOf(localEOL) >= 0) { localText = localText.replace(localEOL,""); }
    localEOL = String.fromCharCode(13, 10, 9); // CR+LF+TAB
    while (localText.indexOf(localEOL) >= 0) { localText = localText.replace(localEOL,""); }
    return localText;
  },

  lineFolding: function(textValue) {
    var lineToFold = String(textValue);
    var foldingEOL = String.fromCharCode(13, 10, 32); // CR+LF+SP
    if (lineToFold.length > 73) {
      lineToFold = lineToFold.substring(0,72) + foldingEOL + this.lineFolding(lineToFold.substring(72,undefined));
    }
    return lineToFold;
  },

  LineAppend: function(textValue, appendValue) {
    var tempText = String(textValue);
    var tempText1 = String(appendValue);
    if (tempText.length > 0) {
      if (tempText1.length > 0) { tempText = tempText + String.fromCharCode(13, 10) + tempText1; }
    } else { if (tempText1.length > 0) { tempText = tempText1; } }
    return tempText;
  },

  LineSplit: function(cardItem) {
    var localCardItem = String(cardItem);
    var returnItem = {tag:"", params:"", value:""};
    var localSplitIdx = localCardItem.indexOf(":");
    var tempText = localCardItem.substring(0,localSplitIdx);
    returnItem.value = localCardItem.substring((localSplitIdx+1),undefined);
    localSplitIdx = tempText.indexOf(";");
    if (localSplitIdx > 0) {
      returnItem.tag = tempText.substring(0,localSplitIdx);
      returnItem.params = tempText.substring((localSplitIdx+1),undefined);
    } else {
      returnItem.tag = tempText;
      returnItem.params = "";
    }
    return returnItem;
  },

  UnescapeChars: function(textLine) {
    var localTextLine = String(textLine);
    var localCharOffset = 0;
    var localCharPos = localTextLine.indexOf(String.fromCharCode(92),localCharOffset);
    var localSubString1 = "";
    var localSubString2 = "";
    while (localCharPos >= 0) {
      localSubString1 = localTextLine.substring(0,localCharPos);
      localSubString2 = localTextLine.substring((localCharPos+2),undefined);
      switch (localTextLine.charAt((localCharPos+1))) {
        case ",": localTextLine = String.concat(localSubString1,",",localSubString2);
        break;
        case ";": localTextLine = String.concat(localSubString1,";",localSubString2);
        break;
        case String.fromCharCode(92): localTextLine = String.concat(localSubString1,String.fromCharCode(92),localSubString2);
        break;
        case "n":
        case "N":
          localTextLine = String.concat(localSubString1,"\n",localSubString2);
        break;
      }
      localCharOffset = localCharPos + 1;
      localCharPos = localTextLine.indexOf(String.fromCharCode(92),localCharOffset);
    }
    return localTextLine;
  },

  UpperCaseInitialLetter: function(textLine) {
    var tempText = String(textLine).toLowerCase();
    var tempArray = tempText.split(",");
    var tempIdx = 0;
    for (tempIdx = 0; tempIdx < tempArray.length; tempIdx++) {
      tempText = String(tempArray[tempIdx]).substring(0,1).toUpperCase();
      tempText = tempText + String(tempArray[tempIdx]).substring(1,undefined);
      tempArray[tempIdx] = tempText;
    }
    tempText = tempArray.join(",");
    return tempText;
  },

  GeoObject: function(geoItem) {
    var localGeoItem = String(geoItem);
    var localGeoObject = {latdeg:"0", latmin:"0", latsec:"0.00", londeg:"0", lonmin:"0", lonsec:"0.00"};
    if (localGeoItem.length > 0) {
      var tempFloat = 0.0;
      var tempDeg = 0.0;
      var tempMin = 0.0;
      var tempSec = 0.0;
      var negValue = false;
      var localGeoArray = localGeoItem.split(";");
      if (localGeoArray.length > 0) {
        // Latitude Conversion
        tempFloat = parseFloat(localGeoArray[0]);
        if (tempFloat < 0) { negValue = true; tempFloat = -1.0 * tempFloat; }
        if (isFinite(tempFloat) == true) {
          tempDeg = parseInt(tempFloat);
          tempFloat = 60.0*(tempFloat - tempDeg);
          tempMin = parseInt(tempFloat);
          tempSec  = 6000.0*(tempFloat - tempMin);
          tempSec = parseInt(tempSec);
          tempSec = parseFloat(tempSec) / 100.0;
          if (negValue == false) { localGeoObject.latdeg = String(tempDeg);
          } else { localGeoObject.latdeg = "-"+String(tempDeg); }
          localGeoObject.latmin = String(tempMin);
          localGeoObject.latsec = String(tempSec);
        }
      }
      if (localGeoArray.length > 1) {
        // Longitude conversion
        tempFloat = parseFloat(localGeoArray[1]);
        if (tempFloat < 0) { negValue = true; tempFloat = -1.0 * tempFloat; } else { negValue = true; }
        if (isFinite(tempFloat) == true) {
          tempDeg = parseInt(tempFloat);
          tempFloat = 60.0*(tempFloat - tempDeg);
          tempMin  = parseInt(tempFloat);
          tempSec  = 6000.0*(tempFloat - tempMin);
          tempSec = parseInt(tempSec);
          tempSec = parseFloat(tempSec) / 100.0;
          if (negValue == false) { localGeoObject.londeg = String(tempDeg);
            } else { localGeoObject.londeg = "-"+String(tempDeg); }
          localGeoObject.lonmin = String(tempMin);
          localGeoObject.lonsec = String(tempSec);
        }
      }
    }
    return localGeoObject;
  },

  SafeSplitOnChar: function(textLine, textChar) {
    var localItemArray = new Array();
    var localEscapedArray = new Array();
    var localUNescapedArray = new Array();
    var searchChar = String.fromCharCode(92) + textChar;
    var localDelim1 = 0;
    var localDelim2 = textLine.indexOf(searchChar,localDelim1);
    while (localDelim2 >= 0) { // Find any escaped versions of the character
      localEscapedArray.push((localDelim2+1));
      localDelim1 = localDelim2 + 2;
      localDelim2 = textLine.indexOf(searchChar,localDelim1);
    }
    searchChar = textChar;
    localDelim1 = 0;
    localDelim2 = textLine.indexOf(searchChar,localDelim1);
    while (localDelim2 >= 0) { // Find any UNescaped versions of the character
      if (localEscapedArray.indexOf(localDelim2) == -1) { localUNescapedArray.push(localDelim2); }
      localDelim1 = localDelim2 + 1;
      localDelim2 = textLine.indexOf(searchChar,localDelim1);
    }
    if (localUNescapedArray.length > 0) { // Now split the line up
      localDelim1 = 0;
      for (localDelim2 = 0; localDelim2 < localUNescapedArray.length; localDelim2++) {
        localItemArray.push(cdbcommon.UnescapeChars(textLine.substring(localDelim1,localUNescapedArray[localDelim2])));
        localDelim1 = localUNescapedArray[localDelim2] + 1;
      }
      if (localDelim1 < textLine.length) { // Tack on the last part of the line if it is missing
        localItemArray.push(cdbcommon.UnescapeChars(textLine.substring(localDelim1,undefined)));
      }
    }
    return localItemArray;
  },

  ParamCheck: function(currParams, defaultParams) {
    var updatedParams = "";
    var tempParams = String(currParams).toLowerCase();
    while (tempParams.indexOf("type=") >= 0) { tempParams = tempParams.replace("type=",""); }
    if (tempParams.length > 0) {
      var definedPref = false;
      if (tempParams.indexOf("pref") >= 0) { definedPref = true; }
      var paramArray = tempParams.split(",");
      var tempIdx = 0;
      if (definedPref == true) {
        tempIdx = paramArray.indexOf("pref");
        if (tempIdx >= 0) { paramArray.splice(tempIdx,1); }
      }
      if (paramArray.length > 0) {
        var tempText = "";
        for (tempIdx = 0; tempIdx < paramArray.length; tempIdx++) {
          tempText = String(paramArray[tempIdx]).substring(0,1).toUpperCase();
          tempText = tempText + String(paramArray[tempIdx]).substring(1,undefined);
          paramArray[tempIdx] = tempText;
        }
        updatedParams = paramArray.join(",");
      } else { updatedParams = String(defaultParams); }
      if (definedPref == true) { if (updatedParams.length > 0) {
        updatedParams = updatedParams + ",Pref";
      } else { updatedParams = "Pref"; } }
    } else { updatedParams = String(defaultParams); }
    return updatedParams;
  }
}
