Components.utils.import("resource://gre/modules/Services.jsm");

var cdbdisplayJS = {
  cdbPhotoFile : null,
  cdbLogoFile : null,
  cdbCardEntry : null,
  cdbAppID: null,

  genChunk: function() {
    var tempCount = 0;
    var tempRandom = "";
    tempCount = Math.floor(65536*Math.random());
    tempRandom = tempCount.toString(16);
    while (tempRandom.length < 4) { tempRandom = "0"+tempRandom; }
    return tempRandom;
  },

  GenerateUUID: function() {
    var localUUID = null;
    var tempUUID = null;
    try {
      tempUUID = Components.classes["@mozilla.org/uuid-generator;1"]
                           .getService(Components.interfaces.nsIUUIDGenerator)
                           .generateUUID();
      localUUID = String(tempUUID).substring(1,(String(tempUUID).length - 1));
    } catch(e) {
      var tempCount = 0;
      var tempRandom = "";
      tempUUID = "";
      // time_low
      tempUUID = this.genChunk();
      tempUUID = tempUUID + this.genChunk();
      tempUUID = tempUUID + "-";
      // time_mid
      tempUUID = tempUUID + this.genChunk();
      tempUUID = tempUUID + "-";
      // time_hi_and_version
      tempCount = Math.floor(4096*Math.random());
      tempRandom = tempCount.toString(16);
      while (tempRandom.length < 3) { tempRandom = "0"+tempRandom; }
      tempUUID = tempUUID + "4" + tempRandom;
      // clock_seq_and_reserved and clock_seq_low
      tempCount = 8 + Math.floor(4*Math.random());
      tempRandom = tempCount.toString(16);
      tempUUID = tempUUID + tempRandom;
      tempCount = Math.floor(4096*Math.random());
      tempRandom = tempCount.toString(16);
      while (tempRandom.length < 3) { tempRandom = "0"+tempRandom; }
      tempUUID = tempUUID + tempRandom;
      tempUUID = tempUUID + "-";
      // node
      tempUUID = tempUUID + this.genChunk();
      tempUUID = tempUUID + this.genChunk();
      tempUUID = tempUUID + this.genChunk();
      localUUID = tempUUID;
    }
    return localUUID;
  },

  SaveImage: function(imageObject) {
    var returnItem = null;
    var localFilename = String(imageObject.filename);
    var localFileData = String(imageObject.data);
    if ((localFilename.length > 0) && (localFileData.length > 0)) {
      var imageFile = Services.dirsvc.get("TmpD",Components.interfaces.nsIFile);
      var imageStream = Components.classes["@mozilla.org/network/safe-file-output-stream;1"]
                                  .createInstance(Components.interfaces.nsIFileOutputStream);
      try {
        imageFile.append(localFilename);
        imageFile.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE,parseInt("0600",8));
        imageStream.init(imageFile, 0x04 | 0x08 | 0x20, parseInt("0600",8), 0);
        var binaryData = window.atob(localFileData);
        imageStream.write(binaryData,binaryData.length);
        if (imageStream instanceof Components.interfaces.nsISafeOutputStream) {
          imageStream.finish(); } else { imageStream.close(); }
        returnItem = imageFile;
      } catch(e) { returnItem = null; }
    }
    return returnItem;
  },

  SaveAs: function() {
    var localCard = String(cdbdisplayJS.cdbCardEntry);
    if (localCard.length > 0) {
      var localPickerInterface = Components.interfaces.nsIFilePicker;
      var localFilePicker = Components.classes["@mozilla.org/filepicker;1"].createInstance(localPickerInterface);
      var tempText = document.getElementById("cdbdisplayStrings").getString("cdbExportContactTitle");
      localFilePicker.init(window, tempText, localPickerInterface.modeSave);
      tempText = document.getElementById("cdbdisplayStrings").getString("cdbExportContactFilter");
      localFilePicker.appendFilter (tempText,"*.vcard; *.vcf");
      localFilePicker.appendFilters(localPickerInterface.filterAll | localPickerInterface.filterText);
      if (localFilePicker.show() != localPickerInterface.returnCancel) {
        var exportFile = localFilePicker.file;
        var vCardStream = Components.classes["@mozilla.org/network/file-output-stream;1"]
                                    .createInstance(Components.interfaces.nsIFileOutputStream);
        vCardStream.init(exportFile, 0x04 | 0x08 | 0x20, parseInt("0600",8), 0);
        var vCardStreamConverter = Components.classes["@mozilla.org/intl/converter-output-stream;1"]
                                             .createInstance(Components.interfaces.nsIConverterOutputStream);
        vCardStreamConverter.init(vCardStream, "UTF-8", 0, 0);
        vCardStreamConverter.writeString(localCard);
        vCardStreamConverter.close();
        Services.prompt.alert(null,
                              document.getElementById("cdbdisplayStrings").getString("cdbExportContactTitle"),
                              document.getElementById("cdbdisplayStrings").getString("cdbExportContactMsg3"));
      } else {
        Services.prompt.alert(null,
                              document.getElementById("cdbdisplayStrings").getString("cdbExportContactTitle"),
                              document.getElementById("cdbdisplayStrings").getString("cdbExportContactMsg2"));
      }
    } else {
      Services.prompt.alert(null,
                            document.getElementById("cdbdisplayStrings").getString("cdbExportContactTitle"),
                            document.getElementById("cdbdisplayStrings").getString("cdbExportContactMsg1"));
    }
  },

  AddContact: function() {
    var localAppID = cdbdisplayJS.cdbAppID;
    switch (localAppID) {
      case "{92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}": // SeaMonkey
      case "{3550f703-e582-4d05-9a08-453d09bdfdc6}": // Thunderbird
        var localCard = String(cdbdisplayJS.cdbCardEntry);
        if (localCard.length > 0) {
          var addressbookList = Components.classes["@mozilla.org/abmanager;1"]
                                          .getService(Components.interfaces.nsIAbManager)
                                          .directories;
          var abDirectories = new Array();
          var abName = new Array();
          var tempAb = null;
          while (addressbookList.hasMoreElements()) {
            tempAb = addressbookList.getNext().QueryInterface(Components.interfaces.nsIAbDirectory);
            abDirectories.push(tempAb);
            abName.push(tempAb.dirName);
          }
          if (abName.length > 0) {
            var localDialogParams = {abList:null, abSelection:null, doneOK: null};
            localDialogParams.abList = abName;
            var localRetVal = window.openDialog("chrome://carddavbrowser/content/cdbdirselect.xul", "",
                                                "chrome, dialog, modal, resizable=yes", localDialogParams).focus();
            if ((localDialogParams.doneOK == true) && (localDialogParams.abSelection != "undefined")){
              var selectedAb = parseInt(localDialogParams.abSelection);
              if ((isNaN(selectedAb) == false) && (selectedAb >= 0) && (selectedAb < abName.length)) {
                var tempText = "";
                var tempText1 = "";
                var tempArray = null;
                var tempArray1 = null;
                var itemList = null;
                var tempItem1 = null;
                var tempItem2 = null;
                var tempIdx = 0;
                var tempIdx1 = 0;
                tempAb = abDirectories[selectedAb];
                var newCard = Components.classes["@mozilla.org/addressbook/cardproperty;1"]
                                        .createInstance(Components.interfaces.nsIAbCard);
                tempText = String(document.getElementById("vcard-fn-value").getAttribute("value"));
                if (tempText.length > 0) { newCard.setProperty("DisplayName",tempText); } // FN
                tempText = String(document.getElementById("vcard-name-family-value").getAttribute("value"));
                if (tempText.length > 0) { newCard.setProperty("LastName",tempText); } // N - Family Name
                tempText = String(document.getElementById("vcard-name-given-value").getAttribute("value"));
                if (tempText.length > 0) { newCard.setProperty("FirstName",tempText); } // N - Given Name
                tempText = String(document.getElementById("vcard-nickname-value").getAttribute("value"));
                if (tempText.length > 0) { newCard.setProperty("NickName",tempText); } // NICKNAME
                tempText = String(document.getElementById("vcard-website-value").getAttribute("value"));
                if (tempText.length > 0) { newCard.setProperty("WebPage1",tempText); } // URL
                tempText = String(document.getElementById("vcard-title-value").getAttribute("value"));
                if (tempText.length > 0) { newCard.setProperty("JobTitle",tempText); } // TITLE
                tempText = String(document.getElementById("vcard-notes-value").getAttribute("value"));
                if (tempText.length > 0) { newCard.setProperty("Notes",tempText); } // NOTE
                tempText = String(document.getElementById("vcard-dob-day-value").getAttribute("value"));
                if (tempText.length > 0) { newCard.setProperty("BirthDay",tempText); } // BDAY - Day
                tempText = String(document.getElementById("vcard-dob-month-value").getAttribute("value"));
                if (tempText.length > 0) { newCard.setProperty("BirthMonth",tempText); } // BDAY - Month
                tempText = String(document.getElementById("vcard-dob-year-value").getAttribute("value"));
                if (tempText.length > 0) { newCard.setProperty("BirthYear",tempText); } // BDAY - Year
                tempText = String(document.getElementById("vcard-org-value").getAttribute("value"));
                if (tempText.length > 0) { tempArray = cdbcommon.SafeSplitOnChar(tempText,";");
                  while (tempArray.length < 2) { tempArray.push(""); }
                  tempText1 = cdbcommon.UnescapeChars(tempArray[0]);
                  if (tempText1.length > 0) { newCard.setProperty("Company",tempText1); } // ORG - Company
                  tempText1 = cdbcommon.UnescapeChars(tempArray[1]);
                  if (tempText1.length > 0) { newCard.setProperty("Department",tempText1); } // ORG - Department
                }
                if (document.getElementById("vcard-email-grid-rows").hasChildNodes()) {
                  itemList = document.getElementById("vcard-email-grid-rows").childNodes;
                  tempArray = new Array();
                  tempArray1 = new Array();
                  for (tempItem1 of itemList) {
                    tempItem2 = tempItem1.firstChild;
                    tempText = String(tempItem2.getAttribute("value")).toUpperCase();
                    tempItem2 = tempItem1.lastChild;
                    tempText1 = String(tempItem2.getAttribute("value"));
                    if ((tempText.indexOf("X400") < 0) && (tempText.indexOf("-") < 0)) {
                      if (tempText.indexOf("PREF") >= 0) { tempArray.push(tempText1);
                      } else { tempArray1.push(tempText1); }
                    }
                  }
                  if (tempArray.length > 0) { newCard.setProperty("PrimaryEmail",tempArray[0]); // EMAIL
                    if (tempArray.length > 1) { newCard.setProperty("SecondEmail",tempArray[1]); // EMAIL
                    } else {
                      if (tempArray1.length > 0) { newCard.setProperty("SecondEmail",tempArray1[0]); } // EMAIL
                    }
                  } else {
                    if (tempArray1.length > 0) { newCard.setProperty("PrimaryEmail",tempArray1[0]); // EMAIL
                      if (tempArray1.length > 1) { newCard.setProperty("SecondEmail",tempArray1[1]); } // EMAIL
                    }
                  }
                }
                itemList = document.getElementById("vcardedit-phone-listbox").getElementsByTagName("listitem");
                tempArray = ["HOME","WORK","FAX","PAGER","CELL"];
                for (tempItem1 of itemList) {
                  tempItem2 = tempItem1.firstChild;
                  tempText = String(tempItem2.getAttribute("label")).toUpperCase();
                  tempItem2 = tempItem1.lastChild;
                  tempText1 = String(tempItem2.getAttribute("label"));
                  tempIdx = tempArray.indexOf(tempText);
                  if (tempIdx >= 0) { tempArray.splice(tempIdx,1);
                    switch (tempText) {
                      case "HOME": newCard.setProperty("HomePhone",tempText1); break; // TEL - HOME
                      case "WORK": newCard.setProperty("WorkPhone",tempText1); break; // TEL - WORK
                      case "FAX": newCard.setProperty("FaxNumber",tempText1); break; // TEL - FAX
                      case "PAGER": newCard.setProperty("PagerNumber",tempText1); break; // TEL - PAGER
                      case "CELL": newCard.setProperty("CellularNumber",tempText1); break; // TEL - CELL
                    }
                  }
                }
                tempText = String(document.getElementById("vcard-address-grid-rows").getAttribute("vcardhome"));
                if (tempText.length > 0) { tempArray = cdbcommon.SafeSplitOnChar(tempText,";"); // ADR - HOME
                  while (tempArray.length < 7) { tempArray.push(""); }
                  tempText1 = cdbcommon.UnescapeChars(tempArray[1]);
                  newCard.setProperty("HomeAddress",tempText1);
                  tempText1 = cdbcommon.UnescapeChars(tempArray[2]);
                  newCard.setProperty("HomeAddress2",tempText1);
                  tempText1 = cdbcommon.UnescapeChars(tempArray[3]);
                  newCard.setProperty("HomeCity",tempText1);
                  tempText1 = cdbcommon.UnescapeChars(tempArray[4]);
                  newCard.setProperty("HomeState",tempText1);
                  tempText1 = cdbcommon.UnescapeChars(tempArray[5]);
                  newCard.setProperty("HomeZipCode",tempText1);
                  tempText1 = cdbcommon.UnescapeChars(tempArray[6]);
                  newCard.setProperty("HomeCountry",tempText1);
                }
                tempText = String(document.getElementById("vcard-address-grid-rows").getAttribute("vcardwork"));
                if (tempText.length > 0) { tempArray = cdbcommon.SafeSplitOnChar(tempText,";"); // ADR - WORK
                  while (tempArray.length < 7) { tempArray.push(""); }
                  tempText1 = cdbcommon.UnescapeChars(tempArray[1]);
                  newCard.setProperty("WorkAddress",tempText1);
                  tempText1 = cdbcommon.UnescapeChars(tempArray[2]);
                  newCard.setProperty("WorkAddress2",tempText1);
                  tempText1 = cdbcommon.UnescapeChars(tempArray[3]);
                  newCard.setProperty("WorkCity",tempText1);
                  tempText1 = cdbcommon.UnescapeChars(tempArray[4]);
                  newCard.setProperty("WorkState",tempText1);
                  tempText1 = cdbcommon.UnescapeChars(tempArray[5]);
                  newCard.setProperty("WorkZipCode",tempText1);
                  tempText1 = cdbcommon.UnescapeChars(tempArray[6]);
                  newCard.setProperty("WorkCountry",tempText1);
                }
                tempText = String(document.getElementById("vcard-photo-graphic").getAttribute("src"));
                tempIdx = tempText.indexOf("://");
                tempText1 = tempText.substring(0,tempIdx).toUpperCase();
                if (tempText1 != "CHROME") { newCard.setProperty("PhotoURI",tempText);
                  if (tempText1 == "FILE") {  newCard.setProperty("PhotoType","file"); // PHOTO - File
                    if (this.cdbPhotoFile != null) {
                      tempText = this.cdbPhotoFile.leafName;
                      var cardPhoto = Services.dirsvc.get("ProfD",Components.interfaces.nsIFile);
                      cardPhoto.append("Photos");
                      try { this.cdbPhotoFile.copyTo(cardPhoto,tempText); } catch(e) { ; }
                      cardPhoto.append(tempText);
                      newCard.setProperty("PhotoURI","file://"+String(cardPhoto.path));
                    }
                  } else { newCard.setProperty("PhotoType","web"); } // PHOTO - Web
                }
                newCard.setProperty("PreferMailFormat",1); // Defaults to PlainText mail format
                tempText = String(document.getElementById("vcard-other-value").getAttribute("value"));
                tempText = cdbcommon.Unfold(tempText);
                if (tempText.length > 0) {
                  tempArray = ["_GoogleTalk", "_AimScreenName", "_Yahoo", "_Skype",
                               "_QQ", "_MSN", "_ICQ", "_JabberId", "_IRC",
                               "PhoneticFirstName", "PhoneticLastName",
                               "SpouseName", "PreferMailFormat"];
                  tempArray1 = ["X-GOOGLE-TALK", "X-AIM", "X-YAHOO", "X-SKYPE",
                                "X-QQ", "X-MSN", "X-ICQ", "X-JABBER", "X-IRC",
                                "X-PHONETIC-FIRST-NAME", "X-PHONETIC-LAST-NAME",
                                "X-SPOUSE", "X-MOZILLA-HTML"];
                  var itemArray = tempText.split(String.fromCharCode(13, 10));
                  for (tempIdx = 0; tempIdx < itemArray.length; tempIdx++) {
                    tempItem1 = cdbcommon.LineSplit(tempText1); // returnItem = {tag:"", params:"", value:""};
                    tempText1 = tempItem1.tag;
                    tempIdx1 = tempText1.indexOf(".");
                    if (tempIdx1 >= 0) {
                      tempText1 = tempText1.substring((tempIdx1+1),undefined).toUpperCase();
                    } else { tempText1 = tempText1.toUpperCase(); }
                    tempIdx1 = tempArray1.indexOf(tempText1);
                    if (tempIdx1 >= 0) {
                      if (tempText1 == "X-MOZILLA-HTML") { tempItem1.value = String(tempItem1.value).toUpperCase();
                        if ((tempItem1.value != "FALSE") && (tempItem1.value != "0")) {
                          newCard.setProperty("PreferMailFormat",2); }
                      } else { newCard.setProperty(tempArray[tempIdx1],cdbcommon.UnescapeChars(tempItem1.value)); }
                    }
                  }
                }
                newCard = tempAb.addCard(newCard);
                Services.prompt.alert(null,
                                      document.getElementById("cdbdisplayStrings").getString("cdbAddContactTitle"),
                                      document.getElementById("cdbdisplayStrings").getString("cdbAddContactMsg4"));
              }
            }
          } else {
            Services.prompt.alert(null,
                                  document.getElementById("cdbdisplayStrings").getString("cdbAddContactTitle"),
                                  document.getElementById("cdbdisplayStrings").getString("cdbAddContactMsg3"));
          }
        } else {
          Services.prompt.alert(null,
                                document.getElementById("cdbdisplayStrings").getString("cdbAddContactTitle"),
                                document.getElementById("cdbdisplayStrings").getString("cdbAddContactMsg2"));
        }
      break;
      default: // Anything else
        Services.prompt.alert(null,
                              document.getElementById("cdbdisplayStrings").getString("cdbAddContactTitle"),
                              document.getElementById("cdbdisplayStrings").getString("cdbAddContactMsg1"));
      break;
    }
  },

  cdbOpenLink: function() {
    var localNode = document.getElementById("cdbdisplayPopupSetMenu").triggerNode;
    if (localNode != null) {
      var localLinkURI = localNode.value;
      var localLinkType = localNode.getAttribute("cdblinktype");
      if ((localLinkType == "url") || (localLinkType =="mailto")) {
        var localAppID = cdbdisplayJS.cdbAppID + ":" + localLinkType;
        switch (localAppID) {
          case "{ec8030f7-c20a-464f-9b0e-13a3a9e97384}:url": // Firefox + URL
          case "{92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}:url": // SeaMonkey + URL
            var localMainWindow = Components.classes["@mozilla.org/appshell/window-mediator;1"]
                                            .getService(Components.interfaces.nsIWindowMediator)
                                            .getMostRecentWindow("navigator:browser");
            localMainWindow.gBrowser.selectedTab = localMainWindow.gBrowser.addTab(localLinkURI);
            localMainWindow.restore();
          break;
          case "{ec8030f7-c20a-464f-9b0e-13a3a9e97384}:mailto": // Firefox + MAILTO
            var localURI = Components.classes["@mozilla.org/network/simple-uri;1"]
                                     .getService(Components.interfaces.nsIURI);
            try { localURI.spec = localLinkURI;
            } catch(e) { localURI.spec = "mailto:"+localLinkURI; }
            Components.classes["@mozilla.org/uriloader/external-protocol-service;1"]
                       .getService(Components.interfaces.nsIExternalProtocolService).loadUrl(localURI);
          break;
          case "{92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}:mailto": // SeaMonkey + MAILTO
          case "{3550f703-e582-4d05-9a08-453d09bdfdc6}:mailto": // Thunderbird + MAILTO
            var localComposeService = Components.classes["@mozilla.org/messengercompose;1"]
                                                .getService(Components.interfaces.nsIMsgComposeService);
            var localIOservice = Components.classes["@mozilla.org/network/io-service;1"]
                                           .getService(Components.interfaces.nsIIOService);
            try {
              localComposeService.OpenComposeWindowWithURI (null, localIOservice.newURI(("mailto:" + localLinkURI), null, null));
            } catch(e) {}
          break;
          case "{3550f703-e582-4d05-9a08-453d09bdfdc6}:url": // Thunderbird + URL
            var localURI = Components.classes["@mozilla.org/network/simple-uri;1"]
                                     .getService(Components.interfaces.nsIURI);
            try { localURI.spec = localLinkURI;
            } catch(e) { localURI.spec = "http://"+localLinkURI; }
            Components.classes["@mozilla.org/uriloader/external-protocol-service;1"]
                       .getService(Components.interfaces.nsIExternalProtocolService).loadUrl(localURI);
          break;
        }
      }
    }
    return false;
  },

  cdbClickLink: function(event) {
    if ('object' === typeof event) {
      if (event.button == 0) {
        var localNode = event.explicitOriginalTarget;
        if ((localNode != null) && (localNode.hasAttribute("cdblinktype") == true)){
          var localLinkURI = localNode.value;
          var localLinkType = localNode.getAttribute("cdblinktype");
          if ((localLinkType == "url") || (localLinkType =="mailto")) {
            var localAppID = cdbdisplayJS.cdbAppID + ":" + localLinkType;
            switch (localAppID) {
              case "{92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}:mailto": // SeaMonkey + MAILTO
              case "{3550f703-e582-4d05-9a08-453d09bdfdc6}:mailto": // Thunderbird + MAILTO
                var localComposeService = Components.classes["@mozilla.org/messengercompose;1"]
                                                    .getService(Components.interfaces.nsIMsgComposeService);
                var localIOservice = Components.classes["@mozilla.org/network/io-service;1"]
                                               .getService(Components.interfaces.nsIIOService);
                try {
                  localComposeService.OpenComposeWindowWithURI (null, localIOservice.newURI(("mailto:" + localLinkURI), null, null));
                } catch(e) {}
              break;
              case "{3550f703-e582-4d05-9a08-453d09bdfdc6}:url": // Thunderbird + URL
                var localURI = Components.classes["@mozilla.org/network/simple-uri;1"]
                                         .getService(Components.interfaces.nsIURI);
                try { localURI.spec = localLinkURI;
                } catch(e) { localURI.spec = "http://"+localLinkURI; }
                Components.classes["@mozilla.org/uriloader/external-protocol-service;1"]
                           .getService(Components.interfaces.nsIExternalProtocolService).loadUrl(localURI);
              break;
            }
          }
        }
      }
    }
    return false;
  },

  cdbContextCopyA: function() {
    const gClipboardHelper = Components.classes["@mozilla.org/widget/clipboardhelper;1"]
                                       .getService(Components.interfaces.nsIClipboardHelper);
    var localNodeToCopy = document.getElementById("cdbdisplayPopupSetMenu").triggerNode;
    if (localNodeToCopy == null) { localNodeToCopy = document.getElementById("cdbdisplayPopupSetMenu").triggerNode; }
    if (localNodeToCopy != null) {
      // var localValue = localNodeToCopy.lastChild.getAttribute("value");
      var localValue = localNodeToCopy.getAttribute("value");
      gClipboardHelper.copyString(localValue);
    }
  },

  cdbContextCopyB: function() {
    const gClipboardHelper = Components.classes["@mozilla.org/widget/clipboardhelper;1"]
                                       .getService(Components.interfaces.nsIClipboardHelper);
    var localNodeToCopy = document.getElementById("cdbdisplayPopupSetMenu").triggerNode;
    if (localNodeToCopy == null) { localNodeToCopy = document.getElementById("cdbCardTabPopup").triggerNode; }
    if (localNodeToCopy != null) {
      var localValue = localNodeToCopy.lastChild.getAttribute("label");
      gClipboardHelper.copyString(localValue);
    }
  },

  addressSelect: function() {
    var localItemSelect = document.getElementById("vcard-address-menulist").selectedIndex;
    var localItem = document.getElementById("vcard-address-grid-rows");
    var tempText = "";
    switch (localItemSelect) {
      case 0: tempText = localItem.getAttribute("vcarddom");
      break;
      case 1: tempText = localItem.getAttribute("vcardintl");
      break;
      case 2: tempText = localItem.getAttribute("vcardpostal");
      break;
      case 3: tempText = localItem.getAttribute("vcardparcel");
      break;
      case 4: tempText = localItem.getAttribute("vcardhome");
      break;
      case 5: tempText = localItem.getAttribute("vcardwork");
      break;
    }
    if (String(tempText).length > 0) {
      var tempArray1 = cdbcommon.SafeSplitOnChar(tempText,";");
      while (tempArray1.length < 7) { tempArray1.push(""); }
      document.getElementById("vcard-address-pobox-value").setAttribute("value",cdbcommon.UnescapeChars(tempArray1[0]));
      document.getElementById("vcard-address-extended-value").setAttribute("value",cdbcommon.UnescapeChars(tempArray1[1]));
      document.getElementById("vcard-address-street-value").setAttribute("value",cdbcommon.UnescapeChars(tempArray1[2]));
      document.getElementById("vcard-address-city-value").setAttribute("value",cdbcommon.UnescapeChars(tempArray1[3]));
      document.getElementById("vcard-address-state-value").setAttribute("value",cdbcommon.UnescapeChars(tempArray1[4]));
      document.getElementById("vcard-address-zip-value").setAttribute("value",cdbcommon.UnescapeChars(tempArray1[5]));
      document.getElementById("vcard-address-country-value").setAttribute("value",cdbcommon.UnescapeChars(tempArray1[6]));
    } else {
      document.getElementById("vcard-address-pobox-value").setAttribute("value","");
      document.getElementById("vcard-address-extended-value").setAttribute("value","");
      document.getElementById("vcard-address-street-value").setAttribute("value","");
      document.getElementById("vcard-address-city-value").setAttribute("value","");
      document.getElementById("vcard-address-state-value").setAttribute("value","");
      document.getElementById("vcard-address-zip-value").setAttribute("value","");
      document.getElementById("vcard-address-country-value").setAttribute("value","");
    }
  },

  labelSelect: function() {
    var localItemSelect = document.getElementById("vcard-label-menulist").selectedIndex;
    var localItem = document.getElementById("vcard-label-value");
    var tempText = "";
    switch (localItemSelect) {
      case 0: tempText = localItem.getAttribute("vcarddom");
      break;
      case 1: tempText = localItem.getAttribute("vcardintl");
      break;
      case 2: tempText = localItem.getAttribute("vcardpostal");
      break;
      case 3: tempText = localItem.getAttribute("vcardparcel");
      break;
      case 4: tempText = localItem.getAttribute("vcardhome");
      break;
      case 5: tempText = localItem.getAttribute("vcardwork");
      break;
    }
    localItem.setAttribute("value",tempText);
  },

  cdbCardProcessing: function(vCardText) {
    var localvCardText = String(vCardText);
    cdbdisplayJS.cdbCardEntry = localvCardText;
    var localCardObject = {n:false, fn:false, nickname:false, bday:false,
                           url:false, title:false, role:false, org:false,
                           categories:false, class:false, tz:false, geo:false,
                           note:false, photo:false, logo:false};
    var adrObject = {dom:false, intl:false, postal:false, parcel:false, home:false, work:false};
    var lblObject = {dom:false, intl:false, postal:false, parcel:false, home:false, work:false};
    var emailArray = new Array();
    var telArray = new Array();
    var vCardExtras = "";
    localvCardText = cdbcommon.Unfold(localvCardText);
    var localvCardArray = localvCardText.split(String.fromCharCode(13, 10));
    var localArrayIdx = 0;
    var cardObject = null;
    var cardTag = "";
    var localSplitIdx = 0;
    var tempText = "";
    var tempText1 = "";
    var tempText2 = "";
    var tempText3 = "";
    var tempArray1 = null;
    var tempObject = null;
    for (localArrayIdx = 0; localArrayIdx < localvCardArray.length; localArrayIdx++) {
      tempText1 = String(localvCardArray[localArrayIdx]); tempText1 = tempText1.trim();
      if (tempText1.length > 0) {
        tempText = cdbcommon.lineFolding(tempText1);
        cardObject = cdbcommon.LineSplit(tempText1); // returnItem = {tag:"", params:"", value:""};
        cardTag = cardObject.tag;
        localSplitIdx = cardTag.indexOf(".");
        if (localSplitIdx >= 0) {
          cardTag = cardTag.substring((localSplitIdx+1),undefined).toUpperCase();
        } else { cardTag = cardTag.toUpperCase(); }
        switch (cardTag) {
          case "FN": if (localCardObject.fn == false) {
              localCardObject.fn = true;
              tempText2 = cdbcommon.UnescapeChars(cardObject.value);
              document.getElementById("vcard-fn-value").setAttribute("value",tempText2);
              document.getElementById("vcard-fn-value1").setAttribute("value",tempText2);
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "N": if (localCardObject.n == false) {
              localCardObject.n = true;
              tempArray1 = cdbcommon.SafeSplitOnChar(cardObject.value, ";");
              while (tempArray1.length < 5) { tempArray1.push(""); }
              tempText2 = cdbcommon.UnescapeChars(tempArray1[0]);
              document.getElementById("vcard-n-control-value2").setAttribute("value",tempText2);
              document.getElementById("vcard-name-family-value").setAttribute("value",tempText2);
              tempText2 = cdbcommon.UnescapeChars(tempArray1[1]);
              document.getElementById("vcard-n-control-value1").setAttribute("value",tempText2);
              document.getElementById("vcard-name-given-value").setAttribute("value",tempText2);
              tempText2 = cdbcommon.UnescapeChars(tempArray1[2]);
              document.getElementById("vcard-name-additional-value").setAttribute("value",tempText2);
              tempText2 = cdbcommon.UnescapeChars(tempArray1[3]);
              document.getElementById("vcard-name-prefix-value").setAttribute("value",tempText2);
              tempText2 = cdbcommon.UnescapeChars(tempArray1[4]);
              document.getElementById("vcard-name-suffix-value").setAttribute("value",tempText2);
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "NICKNAME": if (localCardObject.nickname == false) {
              localCardObject.nickname = true;
              tempText2 = cdbcommon.UnescapeChars(cardObject.value);
              document.getElementById("vcard-nickname-value").setAttribute("value",tempText2);
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "BDAY": if (localCardObject.bday == false) {
              localCardObject.bday = true;
              tempText2 = String(cardObject.value).toUpperCase();
              localSplitIdx = tempText2.indexOf("T");
              if (localSplitIdx > 0) { tempText2 = tempText2.substring(0,localSplitIdx); } // Drop the time component
              localSplitIdx = tempText2.indexOf("-");
              if (localSplitIdx >= 0) {
                document.getElementById("vcard-dob-day-value").setAttribute("value",tempText2.substring(8,undefined));
                document.getElementById("vcard-dob-month-value").setAttribute("value",tempText2.substring(5,7));
                document.getElementById("vcard-dob-year-value").setAttribute("value",tempText2.substring(0,4));
              } else {
                document.getElementById("vcard-dob-day-value").setAttribute("value",tempText2.substring(6,undefined));
                document.getElementById("vcard-dob-month-value").setAttribute("value",tempText2.substring(4,6));
                document.getElementById("vcard-dob-year-value").setAttribute("value",tempText2.substring(0,4));
              }
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "URL": if (localCardObject.url == false) {
              localCardObject.url = true;
              tempObject = document.getElementById("vcard-website-value");
              tempText2 = cdbcommon.UnescapeChars(cardObject.value);
              tempObject.setAttribute("value",tempText2);
              tempObject.setAttribute("cdblinktype","url");
              tempObject.setAttribute("context","cdbdisplayPopupSetMenu");
              if (cdbdisplayJS.cdbAppID == "{3550f703-e582-4d05-9a08-453d09bdfdc6}") {
                // This bit is for Thunderbird since we don't want it to open the webpage itself
                tempObject.setAttribute("class","cdbLinkStyle");
              } else {
                // This bit if for Firefox and SeaMonkey since they know what to do with web pages
                tempObject.setAttribute("href",tempText2);
                tempObject.setAttribute("class","text-link");
              }
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "TITLE": if (localCardObject.title == false) {
              localCardObject.title = true;
              tempText2 = cdbcommon.UnescapeChars(cardObject.value);
              document.getElementById("vcard-title-value").setAttribute("value",tempText2);
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "ROLE": if (localCardObject.role == false) {
              localCardObject.role = true;
              tempText2 = cdbcommon.UnescapeChars(cardObject.value);
              document.getElementById("vcard-role-value").setAttribute("value",tempText2);
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "ORG": if (localCardObject.org == false) {
              localCardObject.org = true;
              tempText2 = cdbcommon.UnescapeChars(cardObject.value);
              document.getElementById("vcard-org-value").setAttribute("value",tempText2);
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "CATEGORIES": if (localCardObject.categories == false) {
              localCardObject.categories = true;
              tempText2 = cdbcommon.UnescapeChars(cardObject.value);
              document.getElementById("vcard-categories-value").setAttribute("value",tempText2);
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "CLASS": if (localCardObject.class == false) {
              localCardObject.class = true;
              tempText2 = cdbcommon.UnescapeChars(cardObject.value);
              document.getElementById("vcard-class-value").setAttribute("value",tempText2);
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "TZ": if (localCardObject.tz == false) {
              localCardObject.tz = true;
              tempText2 = cdbcommon.UnescapeChars(cardObject.value);
              document.getElementById("vcard-tz-value").setAttribute("value",tempText2);
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "GEO": if (localCardObject.geo == false) {
              localCardObject.geo = true;
              tempObject = cdbcommon.GeoObject(cardObject.value);
              document.getElementById("vcard-lat-deg-value").value = tempObject.latdeg;
              document.getElementById("vcard-lat-min-value").value = tempObject.latmin;
              document.getElementById("vcard-lat-sec-value").value = tempObject.latsec;
              document.getElementById("vcard-long-deg-value").value = tempObject.londeg;
              document.getElementById("vcard-long-min-value").value = tempObject.lonmin;
              document.getElementById("vcard-long-sec-value").value = tempObject.lonsec;
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "NOTE": if (localCardObject.note == false) {
              localCardObject.note = true;
              tempText2 = cdbcommon.UnescapeChars(cardObject.value);
              document.getElementById("vcard-notes-value").setAttribute("value",tempText2);
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "PHOTO": if (localCardObject.photo == false) {
              localCardObject.photo = true;
              tempText2 = String(cardObject.params).toUpperCase();
              localSplitIdx = tempText2.indexOf("URI");
              if (localSplitIdx >= 0) {
                tempText2 = document.getElementById("cdbdisplayStrings").getString("cdbImageType1");
                document.getElementById("vcard-photo-image-type").setAttribute("value",tempText2);
                tempText2 = cdbcommon.UnescapeChars(cardObject.value);
                document.getElementById("vcard-photo-graphic").setAttribute("src",tempText2);
              } else {
                tempObject = {filename:"", data:""};
                tempObject.filename = ".jpg";
                if (tempText2.indexOf("PNG") >= 0) { tempObject.filename = ".png"; }
                if (tempText2.indexOf("GIF") >= 0) { tempObject.filename = ".gif"; }
                tempObject.filename = "Photo-" + this.GenerateUUID() + tempObject.filename;
                tempObject.data = cdbcommon.UnescapeChars(cardObject.value);
                tempObject = this.SaveImage(tempObject);
                if (tempObject != null) {
                  tempText2 = document.getElementById("cdbdisplayStrings").getString("cdbImageType2");
                  document.getElementById("vcard-photo-image-type").setAttribute("value",tempText2);
                  cdbdisplayJS.cdbPhotoFile = tempObject;
                  tempText2 = "file://"+String(tempObject.path);
                  document.getElementById("vcard-photo-graphic").setAttribute("src",tempText2);
                }
              }
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "LOGO": if (localCardObject.logo == false) {
              localCardObject.logo = true;
              tempText2 = String(cardObject.params).toUpperCase();
              localSplitIdx = tempText2.indexOf("URI");
              if (localSplitIdx >= 0) {
                tempText2 = document.getElementById("cdbdisplayStrings").getString("cdbImageType1");
                document.getElementById("vcard-logo-image-type").setAttribute("value",tempText2);
                tempText2 = cdbcommon.UnescapeChars(cardObject.value);
                document.getElementById("vcard-logo-graphic").setAttribute("src",tempText2);
              } else {
                tempObject = {filename:"", data:""};
                tempObject.filename = ".jpg";
                if (tempText2.indexOf("PNG") >= 0) { tempObject.filename = ".png"; }
                if (tempText2.indexOf("GIF") >= 0) { tempObject.filename = ".gif"; }
                tempObject.filename = "Logo-" + this.GenerateUUID() + tempObject.filename;
                tempObject.data = cdbcommon.UnescapeChars(cardObject.value);
                tempObject = this.SaveImage(tempObject);
                if (tempObject != null) {
                  tempText2 = document.getElementById("cdbdisplayStrings").getString("cdbImageType2");
                  document.getElementById("vcard-logo-image-type").setAttribute("value",tempText2);
                  cdbdisplayJS.cdbPhotoFile = tempObject;
                  tempText2 = "file://"+String(tempObject.path);
                  document.getElementById("vcard-logo-graphic").setAttribute("src",tempText2);
                }
              }
            } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
          break;
          case "EMAIL": tempText2 = String(cardObject.params).toUpperCase();
            tempText2 = cdbcommon.ParamCheck(tempText2,"Internet") + ":" + cardObject.value;
            emailArray.push(tempText2);
          break;
          case "TEL": tempText2 = String(cardObject.params).toLowerCase();
            while (tempText2.indexOf("type=") >= 0) { tempText2 = tempText2.replace("type=",""); }
            tempArray1 = tempText2.split(",");
            localSplitIdx = tempArray1.indexOf("pref");
            if (localSplitIdx >= 0) {
              tempArray1.splice(localSplitIdx,1);
              tempText2 = "Pref";
            } else { tempText2 = ""; }
            if (tempArray1.length == 0) { tempArray1.push("Voice"); }
            for (localSplitIdx = 0; localSplitIdx < tempArray1.length; localSplitIdx++) {
              tempText3 = tempArray1[localSplitIdx];
              if (tempText3.length > 0) {
                if (tempText2.length > 0) {
                  tempText3 = tempText3 + "," + tempText2 + ":" + cardObject.value;
                } else { tempText3 = tempText3 + ":" + cardObject.value; }
                telArray.push(tempText3);
              }
            }
          break;
          case "ADR": tempText2 = String(cardObject.params).toLowerCase();
            while (tempText2.indexOf("type=") >= 0) { tempText2 = tempText2.replace("type=",""); }
            tempArray1 = tempText2.split(",");
            localSplitIdx = tempArray1.indexOf("pref");
            if (localSplitIdx >= 0) { tempArray1.splice(localSplitIdx,1); }
            for (localSplitIdx = 0; localSplitIdx < tempArray1.length; localSplitIdx++) {
              tempText3 = tempArray1[localSplitIdx];
              if (tempText3.indexOf("language=") >= 0) { tempArray1.splice(localSplitIdx,1); }
            }
            if (tempArray1.length == 0) {
              tempArray1.push("intl");
              tempArray1.push("postal");
              tempArray1.push("parcel");
              tempArray1.push("work");
            }
            tempObject = document.getElementById("vcard-address-grid-rows");
            for (localSplitIdx = 0; localSplitIdx < tempArray1.length; localSplitIdx++) {
              switch (tempArray1[localSplitIdx]) {
                case "dom": if (adrObject.dom == false) {
                    adrObject.dom = true;
                    tempObject.setAttribute("vcarddom",cardObject.value);
                  } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
                break;
                case "intl": if (adrObject.intl == false) {
                    adrObject.intl = true;
                    tempObject.setAttribute("vcardintl",cardObject.value);
                  } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
                break;
                case "postal": if (adrObject.postal == false) {
                    adrObject.postal = true;
                    tempObject.setAttribute("vcardpostal",cardObject.value);
                  } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
                break;
                case "parcel": if (adrObject.parcel == false) {
                    adrObject.parcel = true;
                    tempObject.setAttribute("vcardparcel",cardObject.value);
                  } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
                break;
                case "home": if (adrObject.home == false) {
                    adrObject.home = true;
                    tempObject.setAttribute("vcardhome",cardObject.value);
                  } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
                break;
                case "work": if (adrObject.work == false) {
                    adrObject.work = true;
                    tempObject.setAttribute("vcardwork",cardObject.value);
                  } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
                break;
              }
            }
          break;
          case "LABEL": tempText2 = String(cardObject.params).toLowerCase();
            while (tempText2.indexOf("type=") >= 0) { tempText2 = tempText2.replace("type=",""); }
            tempArray1 = tempText2.split(",");
            localSplitIdx = tempArray1.indexOf("pref");
            if (localSplitIdx >= 0) { tempArray1.splice(localSplitIdx,1); }
            for (localSplitIdx = 0; localSplitIdx < tempArray1.length; localSplitIdx++) {
              tempText3 = tempArray1[localSplitIdx];
              if (tempText3.indexOf("language=") >= 0) { tempArray1.splice(localSplitIdx,1); }
            }
            if (tempArray1.length == 0) {
              tempArray1.push("intl");
              tempArray1.push("postal");
              tempArray1.push("parcel");
              tempArray1.push("work");
            }
            tempObject = document.getElementById("vcard-label-value");
            tempText2 = cdbcommon.UnescapeChars(cardObject.value);
            for (localSplitIdx = 0; localSplitIdx < tempArray1.length; localSplitIdx++) {
              switch (tempArray1[localSplitIdx]) {
                case "dom": if (lblObject.dom == false) {
                    lblObject.dom = true;
                    tempObject.setAttribute("vcarddom",tempText2);
                  } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
                break;
                case "intl": if (lblObject.intl == false) {
                    lblObject.intl = true;
                    tempObject.setAttribute("vcardintl",tempText2);
                  } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
                break;
                case "postal": if (lblObject.postal == false) {
                    lblObject.postal = true;
                    tempObject.setAttribute("vcardpostal",tempText2);
                  } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
                break;
                case "parcel": if (lblObject.parcel == false) {
                    lblObject.parcel = true;
                    tempObject.setAttribute("vcardparcel",tempText2);
                  } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
                break;
                case "home": if (lblObject.home == false) {
                    lblObject.home = true;
                    tempObject.setAttribute("vcardhome",tempText2);
                  } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
                break;
                case "work": if (lblObject.work == false) {
                    lblObject.work = true;
                    tempObject.setAttribute("vcardwork",tempText2);
                  } else { vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText); }
                break;
              }
            }
          break;
          case "VERSION":
          case "BEGIN":
          case "END": tempText2 = "";
          break;
          default: vCardExtras = cdbcommon.LineAppend(vCardExtras, tempText);
          break;
        }
      }
    }
    if (vCardExtras.length > 0) { document.getElementById("vcard-other-value").setAttribute("value",vCardExtras); }
    if (emailArray.length > 0) {
      document.getElementById("vcard-email-noentries").setAttribute("hidden","true");
      document.getElementById("vcard-email-grid").setAttribute("hidden","false");
      var emailGridParent = document.getElementById("vcard-email-grid-rows");
      var emailGridRow = null;
      var emailGridCell = null;
      for (localArrayIdx = 0; localArrayIdx < emailArray.length; localArrayIdx++) {
        tempText = String(localArrayIdx);
        tempText1 = "";
        tempText2 = "";
        tempArray1 = String(emailArray[localArrayIdx]).split(":");
        if (tempArray1.length > 1) {
          tempText1 = tempArray1[0];
          tempArray1.splice(0,1);
          tempText2 = tempArray1.join(":");
        } else { tempText2 = tempArray1[0]; }
        emailGridRow = document.createElement("row");
        emailGridRow.setAttribute("id","vcard-email-grid-row-"+tempText);
        emailGridCell = document.createElement("label");
        emailGridCell.setAttribute("id","vcard-email-grid-row-"+tempText+"-label");
        emailGridCell.setAttribute("value",tempText1);
        emailGridRow.appendChild(emailGridCell);
        emailGridCell = document.createElement("label");
        emailGridCell.setAttribute("id","vcard-email-grid-row-"+tempText+"-email");
        emailGridCell.setAttribute("value",tempText2);
        emailGridCell.setAttribute("context","cdbdisplayPopupSetMenu");
        if (cdbdisplayJS.cdbAppID == "{ec8030f7-c20a-464f-9b0e-13a3a9e97384}") {
          // This bit is for Firefox since it doesn't have builtin email - just open the link
          emailGridCell.setAttribute("href","mailto:"+tempText2);
          emailGridCell.setAttribute("class","text-link");
        } else {
          // This bit it for SeaMonkey and Thunderbird as they include email clients
          emailGridCell.setAttribute("class","cdbLinkStyle");
        }
        emailGridCell.setAttribute("cdblinktype","mailto");
        emailGridRow.appendChild(emailGridCell);
        emailGridParent.appendChild(emailGridRow);
      }
    }
    if (telArray.length > 0) {
      document.getElementById("vcard-phone-noentries").setAttribute("hidden","true");
      document.getElementById("vcardedit-phone-listbox").setAttribute("hidden","false");
      var telListParent = document.getElementById("vcardedit-phone-listbox");
      var telListRow = null;
      var telListCell = null;
      for (localArrayIdx = 0; localArrayIdx < telArray.length; localArrayIdx++) {
        tempText = String(localArrayIdx);
        tempText1 = "";
        tempText2 = "";
        tempArray1 = String(telArray[localArrayIdx]).split(":");
        if (tempArray1.length > 1) {
          tempText1 = cdbcommon.UpperCaseInitialLetter(tempArray1[0]);
          tempArray1.splice(0,1);
          tempText2 = tempArray1.join(":");
        } else { tempText2 = tempArray1[0]; }
        telListRow = document.createElement("listitem");
        telListRow.setAttribute("id","vcardedit-phone-listitem-"+tempText);
        telListRow.setAttribute("context","cdbCardTabPopup");
        telListCell = document.createElement("listcell");
        telListCell.setAttribute("id","vcardedit-phone-listcell-a-"+tempText);
        telListCell.setAttribute("label",tempText1);
        telListRow.appendChild(telListCell);
        telListCell = document.createElement("listcell");
        telListCell.setAttribute("id","vcardedit-phone-listcell-b-"+tempText);
        telListCell.setAttribute("label",tempText2);
        telListRow.appendChild(telListCell);
        telListParent.appendChild(telListRow);
      }
    }
    tempText = String(document.getElementById("vcard-address-grid-rows").getAttribute("vcarddom"));
    if (tempText.length > 0) {
      tempArray1 = cdbcommon.SafeSplitOnChar(tempText,";");
      while (tempArray1.length < 7) { tempArray1.push(""); }
      document.getElementById("vcard-address-pobox-value").setAttribute("value",cdbcommon.UnescapeChars(tempArray1[0]));
      document.getElementById("vcard-address-extended-value").setAttribute("value",cdbcommon.UnescapeChars(tempArray1[1]));
      document.getElementById("vcard-address-street-value").setAttribute("value",cdbcommon.UnescapeChars(tempArray1[2]));
      document.getElementById("vcard-address-city-value").setAttribute("value",cdbcommon.UnescapeChars(tempArray1[3]));
      document.getElementById("vcard-address-state-value").setAttribute("value",cdbcommon.UnescapeChars(tempArray1[4]));
      document.getElementById("vcard-address-zip-value").setAttribute("value",cdbcommon.UnescapeChars(tempArray1[5]));
      document.getElementById("vcard-address-country-value").setAttribute("value",cdbcommon.UnescapeChars(tempArray1[6]));
    }
    tempText = String(document.getElementById("vcard-label-value").getAttribute("vcarddom"));
    if (tempText.length > 0) { document.getElementById("vcard-label-value").setAttribute("value",tempText); }
  },

  cdbCardQuery: function(CardDAVdb, vCardID) {
    var localSQLquery = CardDAVdb.createStatement("SELECT vcard FROM vcards WHERE id = :vcardid");
    localSQLquery.params.vcardid = parseInt(vCardID);
    localSQLquery.step();
    var localvCardText = String(localSQLquery.row.vcard);
    localSQLquery.reset();
    localSQLquery.finalize();
    if (localvCardText.length > 0) {
      cdbdisplayJS.cdbCardProcessing(localvCardText);
    } else {
      var msgTitle = document.getElementById("cdbdisplayStrings").getString("cdbdisplayOnLoadErrorTitle");
      var msgText = document.getElementById("cdbdisplayStrings").getString("cdbdisplayCardQueryError");
      Services.prompt.alert(null, msgTitle, msgText);
    }
  },

  onLoad: function() {
    var localCardDB = window.arguments[0].vcarddb;
    var localCardID = window.arguments[0].vcardid;
    cdbdisplayJS.cdbPhotoFile = null;
    cdbdisplayJS.cdbLogoFile = null;
    var localAppInfo = Components.classes["@mozilla.org/xre/app-info;1"]
                        .getService(Components.interfaces.nsIXULAppInfo);
    cdbdisplayJS.cdbAppID = localAppInfo.ID;
    if ((localCardDB != null) && (localCardID != null)) {
      this.cdbCardQuery(localCardDB,localCardID);
    } else {
      var localPromptTitle = document.getElementById("cdbdisplayStrings").getString("cdbdisplayOnLoadErrorTitle");
      var localPromptMessage = document.getElementById("cdbdisplayStrings").getString("cdbdisplayOnLoadErrorMessage");
      Services.prompt.alert(null,localPromptTitle,localPromptMessage);
    }
  },

  cdbClose: function() {
    if (this.cdbPhotoFile != null) { try { this.cdbPhotoFile.remove(false); } finally { this.cdbPhotoFile = null; } }
    if (this.cdbLogoFile != null) { try { this.cdbLogoFile.remove(false); } finally { this.cdbLogoFile = null; } }
  }
}
window.addEventListener("unload", function () { cdbdisplayJS.cdbClose(); }, false);
window.addEventListener("close", function () { cdbdisplayJS.cdbClose(); }, false);
