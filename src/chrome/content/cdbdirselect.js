var cdbdirselectJS = {
  onLoad: function() {
    var abNames = window.arguments[0].abList;
    var tempIdx = 0;
    var menuParent = document.getElementById("dirselect-groupbox-menupopup");
    var menuItem = document.getElementById("dirselect-groupbox-menuitem-null");
    menuParent.removeChild(menuItem);
    for (tempIdx = 0; tempIdx < abNames.length; tempIdx++) {
      menuItem = document.createElement("menuitem");
      menuItem.setAttribute("id","dirselect-groupbox-menuitem-"+String(tempIdx));
      menuItem.setAttribute("label",String(abNames[tempIdx]));
      menuItem.setAttribute("value",String(tempIdx));
      if (tempIdx == 0) { menuItem.setAttribute("selected","true"); }
      menuParent.appendChild(menuItem);
    }
  },

  doOK: function() {
    var tempText = document.getElementById("dirselect-groupbox-menulist").selectedItem.value;
    window.arguments[0].abSelection = String(tempText);
    window.arguments[0].doneOK = true;
    return true;
  }
};
