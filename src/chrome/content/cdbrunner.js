Components.utils.import("resource://gre/modules/Services.jsm");

var cdbRunner = {
  cdbURLchannel: null,
  cdbURLpath: null,
  cdbDBconnection: null,
  cdbDBfile: null,
  cdbAuthType: null,
  cdbAuthUser: null,
  cdbAuthPass: null,

  onLoad: function() {
    this.cdbURLchannel = null;
    this.cdbURLpath = null;
    this.cdbAuthType = 0;
    this.cdbAuthUser = "";
    this.cdbAuthPass = "";
    var localFile = Services.dirsvc.get("ProfD",Components.interfaces.nsIFile);
    try {
      localFile.append("carddavbrowser.sqlite");
      localFile.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE,parseInt("0600",8));
      this.cdbDBfile = localFile;
    } catch(e) { this.cdbDBfile = null; }
    if (this.cdbDBfile != null) {
      try {
        var localDatabase = Services.storage.openDatabase(this.cdbDBfile);
        localDatabase.createTable("vcards",
            "id INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT NOT NULL, fullname TEXT COLLATE NOCASE, firstname TEXT COLLATE NOCASE, lastname TEXT COLLATE NOCASE, vcard TEXT");
      } catch(e) { localDatabase = null; }
      this.cdbDBconnection = localDatabase;
    } else { this.cdbDBconnection = null; }
    var cdbLocalWindow = document.getElementById("cdbRunnerWindow");
    cdbLocalWindow.focus();
  },

  cdbChildNodeCleanup: function(localParentNode) {
    while (localParentNode.firstChild) {
      if (localParentNode.firstChild.hasChildNodes()) {
        this.cdbChildNodeCleanup(localParentNode.firstChild);
      }
      localParentNode.removeChild(localParentNode.firstChild);
    }
  },

  cdbDoubleClick: function() {
    var localTreeNode = document.getElementById("cdbRunnerContactsTree");
    var localSelectedChildIdx = localTreeNode.currentIndex;
    var localTreeChildrenNodes = document.getElementById("cdbRunnerContactsTreeChildren").childNodes;
    if ((localSelectedChildIdx >= 0) && (localSelectedChildIdx < localTreeChildrenNodes.length)) {
      var localDialogParams = {vcarddb:null, vcardid:null};
      localDialogParams.vcarddb = this.cdbDBconnection;
      localDialogParams.vcardid = localTreeChildrenNodes[localSelectedChildIdx].firstChild.getAttribute("cbdvcardid");
      var localRetVal = window.openDialog("chrome://carddavbrowser/content/cdbdisplay.xul", "",
                            "chrome, dialog, modal, resizable=yes", localDialogParams).focus();
    }
  },

// cdbSecurityInformation: This function is based on information available
//   on the Mozilla Developer Network page entitled: How to check the
//   security state of an XMLHTTPRequest over SSL
  cdbSecurityInformation: function(requestChannel) {
    const localNSSErrorsService = Components.interfaces.nsINSSErrorsService;
    var localErrorStatus = requestChannel.channel.QueryInterface(Components.interfaces.nsIRequest).status;
    var localErrorClass = null;
    var localErrorType = "";
    var localErrorName = "";
    var localSecInfoText = "";
    var localSSLInfoText = "";
    var localNSSerror = null;
    if ((localErrorStatus& 0xff0000) === 0x5a0000) {
      try {
        localErrorClass = Components.classes["@mozilla.org/nss_errors_service;1"]
                                    .getService(localNSSErrorsService).getErrorClass(localErrorStatus);
      } catch(ex) {
        localErrorClass = document.getElementById("cdbStrings").getString("cdbErrorType1");
      }
      if (localErrorClass == localNSSErrorsService.ERROR_CLASS_BAD_CERT) {
        localErrorType = document.getElementById("cdbStrings").getString("cdbErrorType2");
      } else {
        localErrorType = document.getElementById("cdbStrings").getString("cdbErrorType1");
      }
      if ((localErrorStatus& 0xffff) < Math.abs(localNSSErrorsService.NSS_SEC_ERROR_BASE)) {
        localNSSerror = Math.abs(localNSSErrorsService.NSS_SEC_ERROR_BASE) - (localErrorStatus& 0xffff);
        switch (localNSSerror) {
          case 11: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName1");
          break;
          case 12: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName2");
          break;
          case 13:
          case 20:
          case 21:
          case 36: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName3");
          break;
          case 90: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName4");
          break;
          case 176: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName5");
          break;
          default: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName6");
          break;
        }
      } else {
        localNSSerror = Math.abs(localNSSErrorsService.NSS_SSL_ERROR_BASE) - (localErrorStatus& 0xffff);
        switch (localNSSerror) {
          case 3: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName7");
          break;
          case 4: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName8");
          break;
          case 8: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName9");
          break;
          case 9: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName10");
          break;
          case 12: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName11");
          break;
          default: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName6");
          break;
        }
      }
    } else {
      localErrorType = document.getElementById("cdbStrings").getString("cdbErrorType3");
      switch (localErrorStatus) {
        case 0x804B000C: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName12");
        break;
        case 0x804B000E: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName13");
        break;
        case 0x804B001E: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName14");
        break;
        case 0x804B0047: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName15");
        break;
        default: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName6");
        break;
      }
    }
    try {
      var localSecInfo = requestChannel.channel.securityInfo;
      if (localSecInfo instanceof Components.interfaces.nsITransportSecurityInfo) {
        localSecInfo.QueryInterface(Components.interfaces.nsITransportSecurityInfo);
        if ((localSecInfo.securityState & Components.interfaces.nsIWebProgressListener.STATE_IS_INSECURE) == Components.interfaces.nsIWebProgressListener.STATE_IS_INSECURE) {
          localSecInfoText = document.getElementById("cdbStrings").getString("cdbSecurityState1");
        } else if ((localSecInfo.securityState & Components.interfaces.nsIWebProgressListener.STATE_IS_BROKEN) == Components.interfaces.nsIWebProgressListener.STATE_IS_BROKEN) {
          localSecInfoText = document.getElementById("cdbStrings").getString("cdbSecurityState2");
        } else {
          localSecInfoText = document.getElementById("cdbStrings").getString("cdbSecurityState3");
        }
      } else {
        localSecInfoText = document.getElementById("cdbStrings").getString("cdbSecurityState4");
      }
      if (localSecInfo instanceof Components.interfaces.nsISSLStatusProvider) {
        var localSSLcertInfo = localSecInfo.QueryInterface(Components.interfaces.nsISSLStatusProvider).SSLStatus.QueryInterface(Components.interfaces.nsISSLStatus).serverCert;
        localSSLInfoText = document.getElementById("cdbStrings").getString("cdbSSLinfoLine1") + "\n\n";
        localSSLInfoText = localSSLInfoText+document.getElementById("cdbStrings").getString("cdbSSLinfoLine2") + "\n\t";
        localSSLInfoText = localSSLInfoText+document.getElementById("cdbStrings").getString("cdbSSLinfoLine4")+" "+localSSLcertInfo.commonName+"\n\t";
        localSSLInfoText = localSSLInfoText+document.getElementById("cdbStrings").getString("cdbSSLinfoLine5")+" "+localSSLcertInfo.organization+"\n\t";
        localSSLInfoText = localSSLInfoText+document.getElementById("cdbStrings").getString("cdbSSLinfoLine6")+" "+localSSLcertInfo.organizationalUnit+"\n\n";
        localSSLInfoText = localSSLInfoText+document.getElementById("cdbStrings").getString("cdbSSLinfoLine3") + "\n\t";
        localSSLInfoText = localSSLInfoText+document.getElementById("cdbStrings").getString("cdbSSLinfoLine4")+" "+localSSLcertInfo.issuerCommonName+"\n\t";
        localSSLInfoText = localSSLInfoText+document.getElementById("cdbStrings").getString("cdbSSLinfoLine5")+" "+localSSLcertInfo.issuerOrgnaization+"\n\t";
        localSSLInfoText = localSSLInfoText+document.getElementById("cdbStrings").getString("cdbSSLinfoLine6")+" "+localSSLcertInfo.issuerOrganizationalUnit+"\n\n";
        localSSLInfoText = localSSLInfoText+document.getElementById("cdbStrings").getString("cdbSSLinfoLine7") + "\n\t";
        try { localSSLInfoText = localSSLInfoText+document.getElementById("cdbStrings").getString("cdbSSLinfoLine8b")+" "+localSSLcertInfo.md5Fingerprint+"\n\t"; } catch(e) { ; }
        try { localSSLInfoText = localSSLInfoText+document.getElementById("cdbStrings").getString("cdbSSLinfoLine8")+" "+localSSLcertInfo.sha1Fingerprint+"\n\t"; } catch(e) { ; }
        try { localSSLInfoText = localSSLInfoText+document.getElementById("cdbStrings").getString("cdbSSLinfoLine8a")+" "+localSSLcertInfo.sha256Fingerprint+"\n\t"; } catch(e) { ; }
        var localSSLvalidity = localSSLcertInfo.validity.QueryInterface(Components.interfaces.nsIX509CertValidity);
        localSSLInfoText = localSSLInfoText+document.getElementById("cdbStrings").getString("cdbSSLinfoLine9")+" "+localSSLvalidity.notBeforeGMT+"\n\t";
        localSSLInfoText = localSSLInfoText+document.getElementById("cdbStrings").getString("cdbSSLinfoLine10")+" "+localSSLvalidity.notAfterGMT+"\n";
      }
    } catch(ex) {
      localSecInfoText = document.getElementById("cdbStrings").getString("cdbSecurityState3");
    }
    var localErrorTitle = "";
    var localErrorMessage = "";
    if (localErrorName.length > 0) { localErrorTitle = localErrorName; }
    if (localErrorType.length > 0) {
      if (localErrorTitle.length > 0) { localErrorTitle = localErrorType + ": " + localErrorTitle;
      } else { localErrorTitle = localErrorType; }
    }
    if (localSSLInfoText.length > 0) { localErrorMessage = localSSLInfoText; }
    if (localSecInfoText.length > 0) {
      if (localErrorMessage.length > 0) { localErrorMessage = localSecInfoText + "\n\n" + localErrorMessage;
      } else { localErrorMessage = localSecInfoText; }
    }
    if (localErrorTitle.length <= 0) { localErrorTitle = document.getElementById("cdbStrings").getString("cdbGenericErrorTitle"); }
    if (localErrorMessage.length <= 0) { localErrorMessage = document.getElementById("cdbStrings").getString("cdbGenericErrorMessage"); }
    Services.prompt.alert(null,localErrorTitle,localErrorMessage);
  },

  cdbCardDisplay: function() {
    var localSQLquery = this.cdbDBconnection.createStatement("SELECT id,fullname,firstname,lastname FROM vcards ORDER BY lastname,firstname,fullname,id");
    var localCardID = 0;
    var localTreeParent = document.getElementById("cdbRunnerContactsTreeChildren");
    var localcdbvcardcount = parseInt(document.getElementById("cdbRunnerContactsTreeChildren")
                                              .getAttribute("cdbvcardcount"));
    if (isFinite(localcdbvcardcount) == false) { localcdbvcardcount = 0; }
    if (localcdbvcardcount < 0) { localcdbvcardcount = 0; }
    var localNewTreeItem = null;
    var localNewTreeRow = null;
    var localNewTreeCell1 = null;
    var localNewTreeCell2 = null;
    var localNewTreeCell3 = null;
    var vCardCount = 0;
    while (localSQLquery.step()) {
      localCardID = parseInt(localSQLquery.row.id);
      localNewTreeItem = document.createElement("treeitem");
      localNewTreeItem.setAttribute("id","cdbtreeitem"+String(localcdbvcardcount));
      localNewTreeRow = document.createElement("treerow");
      localNewTreeRow.setAttribute("id","cdbtreerow"+String(localcdbvcardcount));
      localNewTreeRow.setAttribute("cbdvcardid",String(localCardID));
      localNewTreeCell1 = document.createElement("treecell");
      localNewTreeCell1.setAttribute("id","cdbtreecellA"+String(localcdbvcardcount));
      localNewTreeCell1.setAttribute("label",String(localSQLquery.row.fullname));
      localNewTreeCell2 = document.createElement("treecell");
      localNewTreeCell2.setAttribute("id","cdbtreecellB"+String(localcdbvcardcount));
      localNewTreeCell2.setAttribute("label",String(localSQLquery.row.firstname));
      localNewTreeCell3 = document.createElement("treecell");
      localNewTreeCell3.setAttribute("id","cdbtreecellC"+String(localcdbvcardcount));
      localNewTreeCell3.setAttribute("label",String(localSQLquery.row.lastname));
      localNewTreeRow.appendChild(localNewTreeCell1);
      localNewTreeRow.appendChild(localNewTreeCell2);
      localNewTreeRow.appendChild(localNewTreeCell3);
      localNewTreeItem.appendChild(localNewTreeRow);
      localTreeParent.appendChild(localNewTreeItem);
      localcdbvcardcount = localcdbvcardcount + 1;
      vCardCount = vCardCount + 1;
    }
    localSQLquery.reset();
    localSQLquery.finalize();
    if (vCardCount == 1) {
      localNewTreeCell1 = document.getElementById("cdbStrings").getString("cdbURLloadLabel3");
    } else {
      localNewTreeCell1 = document.getElementById("cdbStrings").getString("cdbURLloadLabel4");
    }
    localNewTreeCell1 = " ("+String(vCardCount)+" "+localNewTreeCell1+")";
    document.getElementById("cdbRunnerContactsTreeChildren").setAttribute("cdbvcardcount",String(localcdbvcardcount));
    document.getElementById("cdbRunnerStatusLabel").setAttribute("value",
            (document.getElementById("cdbStrings").getString("cdbURLloadLabel1"))+localNewTreeCell1);
  },

  cdbURLdisconnect: function() {
    var localSQLquery = this.cdbDBconnection.createStatement("DELETE FROM vcards");
    try {
      localSQLquery.step();
    } finally {
      localSQLquery.reset();
      localSQLquery.finalize();
    }
    this.cdbURLchannel = null;
    this.cdbURLpath = null;
    this.cdbChildNodeCleanup(document.getElementById("cdbRunnerContactsTreeChildren"));
    document.getElementById("cdbRunnerLinkNavURL").removeAttribute("readonly");
    document.getElementById("cdbRunnerLinkNavConnect").setAttribute("disabled","false");
    document.getElementById("cdbRunnerLinkNavDisonnect").setAttribute("disabled","true");
    document.getElementById("cdbRunnerStatusLabel").setAttribute("value","");
  },

  cdbTimeOut: function() {
    cdbRunner.cdbURLdisconnect();
    var localPromptTitle = document.getElementById("cdbStrings").getString("cdbURLconnectPromptTitle");
    var localPromptMessage = document.getElementById("cdbStrings").getString("cdbURLtimeout");
    Services.prompt.alert(null,localPromptTitle,localPromptMessage);
  },

  cdbNetProgress: function(channelData) {
    if (channelData.status == 401) {
      channelData.abort();
      var localPromptTitle = document.getElementById("cdbStrings").getString("cdbURLconnectPromptTitle");
      var localPromptMessage = document.getElementById("cdbStrings").getString("cdbURLauthFail");
      document.getElementById("cdbRunnerStatusLabel").setAttribute("value","");
      Services.prompt.alert(null,localPromptTitle,localPromptMessage);
    }
  },

  LoadContacts: function(channelData) {
    var localCharset = String(channelData.getResponseHeader("Content-Type")).toLowerCase();
    var localStatus = channelData.status;
    var tempText = "";
    var tempText1 = "";
    if ((localCharset.indexOf("utf-8",0) >= 0) &&
        ((localStatus == 200) || (localStatus == 206) || (localStatus == 207))) {
      // MUST have something to process so only accept 200:OK, 206:Partial Content and 207:Multi-Status
      // Initial processing: Just pull the vCard text, FN and N elements.
      var responseElements = channelData.responseXML.documentElement.getElementsByTagNameNS("DAV:","response");
      if (responseElements.length > 0) { // Only bother doing something if there are entries to process
        document.getElementById("cdbRunnerStatusLabel").setAttribute("value",
                 document.getElementById("cdbStrings").getString("cdbURLstatus4"));
        var localvCardArray = new Array();
        var currItem = null;
        var itemCard = "";
        var itemStatus = "";
        var localCounter = 0;
        var localCounter1 = 0;
        var tempIdx = 0;
        var tempArray1 = null;
        var tempArray2 = null;
        var cardObject = null;
        // Extract the vCards
        for (localCounter = 0; localCounter < responseElements.length; localCounter++) {
          currItem = responseElements[localCounter];
          itemCard = currItem.getElementsByTagNameNS("urn:ietf:params:xml:ns:carddav","address-data")[0].textContent;
          itemStatus = currItem.getElementsByTagNameNS("DAV:","status")[0].textContent;
          if (String(itemStatus).indexOf("200") >= 0) { localvCardArray.push(String(itemCard)); }
        }
        // Update the status bar to show how many vCards are being processed
        tempText = document.getElementById("cdbStrings").getString("cdbURLloadLabel2");
        tempText = tempText + " " + String(localvCardArray.length) + " ";
        if (localvCardArray.length == 1) {
          tempText = tempText + document.getElementById("cdbStrings").getString("cdbURLloadLabel3");
        } else {
          tempText = tempText + document.getElementById("cdbStrings").getString("cdbURLloadLabel4");
        }
        document.getElementById("cdbRunnerStatusLabel").setAttribute("value",tempText+"...");
        // Process the vCards
        var localFullname = "";
        var localFirstname = "";
        var localLastname = "";
        cdbRunner.cdbDBconnection.beginTransaction();
        var localSQLquery = cdbRunner.cdbDBconnection.createStatement("INSERT INTO vcards (fullname,firstname,lastname,vcard) values (:fullname,:firstname,:lastname,:vcard)");
        for (localCounter = 0; localCounter < localvCardArray.length; localCounter++) {
          itemCard = cdbcommon.EnforceLineEndgings(localvCardArray[localCounter]);
          currItem = cdbcommon.Unfold(itemCard);
          tempArray1 = currItem.split(String.fromCharCode(13, 10));
          localFullname = "";
          localFirstname = "";
          localLastname = "";
          for (localCounter1 = 0; localCounter1 < tempArray1.length; localCounter1++) {
            tempText = String(tempArray1[localCounter1]).trim();
            cardObject = cdbcommon.LineSplit(tempText);
            tempText = String(cardObject.tag).toUpperCase();
            tempIdx = tempText.indexOf(".");
            if (tempIdx >= 0) { tempText = tempText.substring((tempIdx+1),undefined); }
            if (tempText == "FN") {
              localFullname = cdbcommon.UnescapeChars(cardObject.value);
            } else if (tempText == "N") {
              tempArray2 = cdbcommon.SafeSplitOnChar(cardObject.value,";");
              switch (tempArray2.length) {
              case 0: localFirstname = ""; localLastname = "";
                break;
              case 1: localFirstname = ""; localLastname = cdbcommon.UnescapeChars(tempArray2[0]);
                break;
              default: localFirstname = cdbcommon.UnescapeChars(tempArray2[1]);
                       localLastname = cdbcommon.UnescapeChars(tempArray2[0]);
                break;
              }
            }
          }
          localSQLquery.params.fullname = localFullname;
          localSQLquery.params.firstname = localFirstname;
          localSQLquery.params.lastname = localLastname;
          localSQLquery.params.vcard = itemCard;
          localSQLquery.step();
          localSQLquery.reset();
        }
        localSQLquery.finalize();
        cdbRunner.cdbDBconnection.commitTransaction();
        document.getElementById("cdbRunnerStatusLabel").setAttribute("value",
                 document.getElementById("cdbStrings").getString("cdbURLloadLabel1"));
        cdbRunner.cdbCardDisplay();
      } else {
        cdbRunner.cdbURLdisconnect();
        tempText = document.getElementById("cdbStrings").getString("cdbURLloadPromptTitle");
        tempText1 = document.getElementById("cdbStrings").getString("cdbURLconnectPromptMessage8");
        Services.prompt.alert(null,tempText,tempText1);
      }
    } else {
      cdbRunner.cdbURLdisconnect();
      tempText = document.getElementById("cdbStrings").getString("cdbURLloadPromptTitle");
      tempText1 = document.getElementById("cdbStrings").getString("cdbURLconnectPromptMessage6");
      Services.prompt.alert(null,tempText,tempText1);
    }
  },

  cdbURLloadStage2: function(channelData) {
    // Extract the HREF entries for the CardDAV contacts
    var localCharset = String(channelData.getResponseHeader("Content-Type")).toLowerCase();
    var localStatus = channelData.status;
    var tempText = "";
    var tempText1 = "";
    if ((localCharset.indexOf("utf-8",0) >= 0) &&
        ((localStatus == 200) || (localStatus == 206) || (localStatus == 207))) {
      var responseElements = channelData.responseXML.documentElement.getElementsByTagNameNS("DAV:","response");
      var pathLength = String(this.cdbURLpath).length;
      if (responseElements.length > 0) { // Only bother doing something if there are entries to process
        var hrefArray = new Array();
        var tempIdx = 0;
        var currItem = null;
        for (tempIdx = 0; tempIdx < responseElements.length; tempIdx++) {
          currItem = responseElements[tempIdx];
          tempText = String(currItem.getElementsByTagNameNS("DAV:","href")[0].textContent); // HREF
          tempText1 = String(currItem.getElementsByTagNameNS("DAV:","status")[0].textContent); // Item Status
          if ((tempText1.indexOf("200") >= 0) && (tempText.length > pathLength)) { hrefArray.push(tempText); }
        }
        if (hrefArray.length > 0) {
          // Build the query body
          tempText = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"+
                     "<C:addressbook-multiget xmlns:D=\"DAV:\" xmlns:C=\"urn:ietf:params:xml:ns:carddav\">\n"+
                     "  <D:prop>\n    <C:address-data content-type=\"text/vcard\" version=\"3.0\" />\n  </D:prop>\n";
          for (tempIdx = 0; tempIdx < hrefArray.length; tempIdx++) {
            tempText1 = hrefArray[tempIdx];
            tempText = tempText + "  <D:href>" + tempText1 + "</D:href>\n";
          }
          tempText = tempText + "</C:addressbook-multiget>\n";
          document.getElementById("cdbRunnerStatusLabel").setAttribute("value",
                   document.getElementById("cdbStrings").getString("cdbURLstatus3"));
          var stage2Channel = new XMLHttpRequest;
          stage2Channel.addEventListener("load",function(e) { cdbRunner.LoadContacts(stage2Channel); },false);
          stage2Channel.addEventListener("error",function(e) { cdbRunner.cdbSecurityInformation(stage2Channel); },false);
          stage2Channel.addEventListener("timeout",function(e) { cdbRunner.cdbTimeOut(); },false);
          stage2Channel.addEventListener("progress",function(e) { cdbRunner.cdbNetProgress(stage2Channel); },false);
          if (cdbRunner.cdbAuthType == 0) {
            stage2Channel.open("REPORT",this.cdbURLchannel,true);
          } else {
            stage2Channel.open("REPORT",this.cdbURLchannel,true, cdbRunner.cdbAuthUser, cdbRunner.cdbAuthPass);
          }
          stage2Channel.timeout = 60000; // Timeout = 60 seconds
          stage2Channel.setRequestHeader("Depth","1");
          stage2Channel.send(tempText);
        } else {
          cdbRunner.cdbURLdisconnect();
          tempText = document.getElementById("cdbStrings").getString("cdbURLloadPromptTitle");
          tempText1 = document.getElementById("cdbStrings").getString("cdbURLconnectPromptMessage7");
          Services.prompt.alert(null,tempText,tempText1);
        }
      } else {
        cdbRunner.cdbURLdisconnect();
        tempText = document.getElementById("cdbStrings").getString("cdbURLloadPromptTitle");
        tempText1 = document.getElementById("cdbStrings").getString("cdbURLconnectPromptMessage7");
        Services.prompt.alert(null,tempText,tempText1);
      }
    } else {
      cdbRunner.cdbURLdisconnect();
      tempText = document.getElementById("cdbStrings").getString("cdbURLloadPromptTitle");
      tempText1 = document.getElementById("cdbStrings").getString("cdbURLconnectPromptMessage7");
      Services.prompt.alert(null,tempText,tempText1);
    }
  },

  cdbURLloadStage1: function(channelData) {
    // Check the server for CardDAV support
    var localPromptTitle = document.getElementById("cdbStrings").getString("cdbURLconnectPromptTitle");
    var localCardDAVcheck = String(channelData.getResponseHeader("DAV")).indexOf("addressbook",0);
    if (localCardDAVcheck >= 0) {
      var stage1Query = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n"+
                        "<D:propfind xmlns:D=\"DAV:\">\n  <D:prop>\n"+
                        "    <D:getetag />\n  </D:prop>\n</D:propfind>";
      document.getElementById("cdbRunnerLinkNavURL").setAttribute("readonly","true");
      document.getElementById("cdbRunnerLinkNavConnect").setAttribute("disabled","true");
      document.getElementById("cdbRunnerLinkNavDisonnect").setAttribute("disabled","false");
      document.getElementById("cdbRunnerStatusLabel").setAttribute("value",
               document.getElementById("cdbStrings").getString("cdbURLstatus2"));
      var stage1Channel = new XMLHttpRequest;
      stage1Channel.addEventListener("load",function(e) { cdbRunner.cdbURLloadStage2(stage1Channel); },false);
      stage1Channel.addEventListener("error",function(e) { cdbRunner.cdbSecurityInformation(stage1Channel); },false);
      stage1Channel.addEventListener("timeout",function(e) { cdbRunner.cdbTimeOut(); },false);
      stage1Channel.addEventListener("progress",function(e) { cdbRunner.cdbNetProgress(stage1Channel); },false);
      if (cdbRunner.cdbAuthType == 0) {
        stage1Channel.open("PROPFIND",this.cdbURLchannel,true);
      } else {
        stage1Channel.open("PROPFIND",this.cdbURLchannel,true, cdbRunner.cdbAuthUser, cdbRunner.cdbAuthPass);
      }
      stage1Channel.timeout = 60000; // Timeout = 60 seconds
      stage1Channel.setRequestHeader("Depth","1");
      stage1Channel.send(stage1Query);
    } else {
      localPromptMessage = document.getElementById("cdbStrings").getString("cdbURLconnectPromptMessage1");
      Services.prompt.alert(null,localPromptTitle,localPromptMessage);
    }
  },

  cdbAuthCheck: function(requestURL) {
    var localAuthType = parseInt(document.getElementById("cdbAuthRadioGroup").selectedItem.value);
    if (isNaN(localAuthType)) { localAuthType = 0; }
    if (localAuthType == 0) {
      cdbRunner.cdbAuthType = 0;
      document.getElementById("cdbAuthRadioGroup").selectedIndex = 0;
    } else {
      cdbRunner.cdbAuthType = 1;
      localAuthType = 1;
      document.getElementById("cdbAuthRadioGroup").selectedIndex = 1;
      var localUsername = new String();
      var localPassword = new String();
      if ((String(cdbRunner.cdbAuthUser).length == 0) && (String(cdbRunner.cdbAuthPass).length == 0)) {
        try {
          var existingLogins = Services.logins.findLogins({}, localURI.prePath, null, "");
          if (existingLogins.length > 0) {
            localUsername = String(existingLogins[0].username);
            localPassword = String(existingLogins[0].password);
          }
        } catch(e) {
          localUsername = String(cdbRunner.cdbAuthUser);
          localPassword = String(cdbRunner.cdbAuthPass);
        }
      } else {
        localUsername = String(cdbRunner.cdbAuthUser);
        localPassword = String(cdbRunner.cdbAuthPass);
      }
      var authDialogParams = {url:null, username:null, password:null, doneOK:null};
      authDialogParams.url = String(requestURL);
      authDialogParams.username = localUsername;
      authDialogParams.password = localPassword;
      authDialogParams.doneOK = false;
      var localRetVal = window.openDialog("chrome://carddavbrowser/content/cdbauthdialog.xul", "",
                              "chrome, dialog, modal, resizable=yes", authDialogParams).focus();
      if (authDialogParams.doneOK == true) {
        cdbRunner.cdbAuthType = 1;
        cdbRunner.cdbAuthUser = String(authDialogParams.username);
        cdbRunner.cdbAuthPass = String(authDialogParams.password);
      } else {
        cdbRunner.cdbAuthType = 0;
        document.getElementById("cdbAuthRadioGroup").selectedIndex = 0;
      }
    }
  },

  cdbCertErrorHandler: function(requestChannel, requestURI) {
    const localNSSErrorsService = Components.interfaces.nsINSSErrorsService;
    var localErrorStatus = requestChannel.channel.QueryInterface(Components.interfaces.nsIRequest).status;
    var localErrorType = "";
    var localErrorName = "";
    if ((localErrorStatus & 0xff0000) === 0x5a0000) {
      // Security Module
      localErrorType = document.getElementById("cdbStrings").getString("cdbErrorType0");
      if ((localErrorStatus & 0xffff) < Math.abs(localNSSErrorsService.NSS_SEC_ERROR_BASE)) {
        var localTargetSite = String(requestURI.scheme) + "://" + String(requestURI.hostPort);
        var localCertParams = {exceptionAdded: false, sslStatus: localErrorStatus,
                               prefetchCert: true, location: localTargetSite};
        var localRetVal = window.openDialog("chrome://pippki/content/exceptionDialog.xul","",
                                            "chrome,centerscreen,modal",localCertParams);
        if (localCertParams.exceptionAdded == true) {
          cdbRunner.cdbURLconnect();
          localErrorName = "";
        } else {
          localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName17");
        }
      } else {
        var sslErr = Math.abs(localNSSErrorsService.NSS_SSL_ERROR_BASE) - (localErrorStatus & 0xffff);
        switch (sslErr) {
          case 3: // SSL_ERROR_NO_CERTIFICATE, ssl(3)
            localErrorName = 'No Certificate';
            break;
          case 4: // SSL_ERROR_BAD_CERTIFICATE, ssl(4)
            localErrorName = 'Bad Certificate';
            break;
          case 8: // SSL_ERROR_UNSUPPORTED_CERTIFICATE_TYPE, ssl(8)
            localErrorName = 'Unsupported Certificate Type';
            break;
          case 9: // SSL_ERROR_UNSUPPORTED_VERSION, ssl(9)
            localErrorName = 'Unsupported TLS Version';
            break;
          case 12: // SSL_ERROR_BAD_CERT_DOMAIN, ssl(12)
            localErrorName = 'Certificate Domain Mismatch';
            break;
          default:
            localErrorName = 'Generic Security Error';
            break;
        }
      }
    } else {
      // Network Error
      localErrorType = document.getElementById("cdbStrings").getString("cdbErrorType3");
      switch (localErrorStatus) {
        case 0x804B000C: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName12");
        break;
        case 0x804B000E: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName13");
        break;
        case 0x804B001E: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName14");
        break;
        case 0x804B0047: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName15");
        break;
        default: localErrorName = document.getElementById("cdbStrings").getString("cdbErrorName6");
        break;
      }
    }
    if (String(localErrorName).length > 0) { Services.prompt.alert(null,localErrorType,localErrorName); }
  },

  cdbURLconnect: function() {
    var localStr = String(document.getElementById("cdbRunnerLinkNavURL").value);
    var localPromptTitle = document.getElementById("cdbStrings").getString("cdbURLconnectPromptTitle");
    var localPromptMessage = "";
    if (this.cdbDBconnection != null) {
      try {
        var localURIservice = Components.classes["@mozilla.org/network/io-service;1"]
                           .getService(Components.interfaces.nsIIOService);
        var localURI = localURIservice.newURI(localStr,null,null);
        if (localURI.schemeIs("http") || localURI.schemeIs("https")) {
          this.cdbAuthCheck(localStr);
          try {
            document.getElementById("cdbRunnerStatusLabel").setAttribute("value",
                     document.getElementById("cdbStrings").getString("cdbURLstatus1"));
            this.cdbURLchannel = localURI.asciiSpec;
            this.cdbURLpath = localURI.path;
            var LocalChannel = new XMLHttpRequest;
            LocalChannel.addEventListener("load",function(e) { cdbRunner.cdbURLloadStage1(LocalChannel); },false);
            LocalChannel.addEventListener("error",function(e) { cdbRunner.cdbCertErrorHandler(LocalChannel, localURI); },false);
            LocalChannel.addEventListener("timeout",function(e) { cdbRunner.cdbTimeOut(); },false);
            LocalChannel.addEventListener("progress",function(e) { cdbRunner.cdbNetProgress(LocalChannel); },false);
            if (cdbRunner.cdbAuthType == 0) {
              LocalChannel.open("OPTIONS",this.cdbURLchannel,true);
            } else {
              LocalChannel.open("OPTIONS",this.cdbURLchannel,true, cdbRunner.cdbAuthUser, cdbRunner.cdbAuthPass);
            }
            LocalChannel.timeout = 60000; // Timeout = 60 seconds
            LocalChannel.send(null);
          } catch(e) {
            localPromptMessage = document.getElementById("cdbStrings").getString("cdbURLconnectPromptMessage3");
            Services.prompt.alert(null,localPromptTitle,localPromptMessage);
          }
        } else {
          localPromptMessage = document.getElementById("cdbStrings").getString("cdbURLconnectPromptMessage4");
          Services.prompt.alert(null,localPromptTitle,localPromptMessage);
        }
      } catch(e) {
        localPromptMessage = document.getElementById("cdbStrings").getString("cdbURLconnectPromptMessage3");
        Services.prompt.alert(null,localPromptTitle,localPromptMessage);
      }
    } else {
      localPromptMessage = document.getElementById("cdbStrings").getString("cdbURLconnectPromptMessage5");
      Services.prompt.alert(null,localPromptTitle,localPromptMessage);
    }
  },

  cdbClose: function() {
    this.cdbURLchannel = null;
    this.cdbURLpath = null;
    if (this.cdbDBconnection != null) {
      try { this.cdbDBconnection.asyncClose(); } finally { this.cdbDBconnection = null; }
    }
    if (this.cdbDBfile != null) {
      try { this.cdbDBfile.remove(false); } finally { this.cdbDBfile = null; }
    }
  }
}

window.addEventListener("load", function () { cdbRunner.onLoad(); }, false);
window.addEventListener("unload", function () { cdbRunner.cdbClose(); }, false);
window.addEventListener("close", function () { cdbRunner.cdbClose(); }, false);
