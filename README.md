# CardDAV Browser



## Description
CardDAV Browser gives you a quick and easy way to connect to a CardDAV server and view vCard details.

## Installation
Install directly from the [CardDAV Browser](https://addons.thunderbird.net/en-US/seamonkey/addon/carddav-browser/versions/?page=1#version-6.2) Extension page.

## License
[Mozilla Public License, version 2.0](http://www.mozilla.org/MPL/2.0/)

## Screenshots

CardDAV Browser Empty Dialog

![CardDAV Browser Empty Dialog](screenshots/cdb-01.png)


CardDAV Browser Manual Authentication Dialog

![CardDAV Browser Manual Authentication Dialog](screenshots/cdb-02.png)


CardDAV Browser With vCards Loaded

![CardDAV Browser With vCards Loaded](screenshots/cdb-03.png)


vCard Details - Contact Tab

![vCard Details - Contact Tab](screenshots/cdb-04.png)


vCard Details - Details Tab

![vCard Details - Details Tab](screenshots/cdb-05.png)


vCard Details - Address Tab

![vCard Details - Address Tab](screenshots/cdb-06.png)


vCard Details - Misc. Tab

![vCard Details - Misc. Tab](screenshots/cdb-07.png)


vCard Details - Notes Tab

![vCard Details - Notes Tab](screenshots/cdb-08.png)


vCard Details - Graphics Tab

![vCard Details - Graphics Tab](screenshots/cdb-09.png)


vCard Details - Other Details Tab

![vCard Details - Other Details Tab](screenshots/cdb-10.png)
